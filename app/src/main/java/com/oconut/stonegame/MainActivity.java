package com.oconut.stonegame;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends Activity{
    /** Called when the activity is first created. */
    private static final String DEBUG_TAG ="{log_android}";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridView gridview = (GridView) findViewById(R.id.GridView);
        gridview.setAdapter(new ImageAdapter(this));

        gridview.setOnItemClickListener(new OnItemClickListener(){
            public void onItemclick(AdapterView<?> parent, View v, int position, long id){
                Toast.makeText(MainActivity.this, "" + position, Toast.LENGTH_SHORT).show();
                Log.i(DEBUG_TAG, "click1111111111111");
            }

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                Log.i(DEBUG_TAG, "click22222222222");
                Toast.makeText(MainActivity.this, "stone"+arg2, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*public void onClick(View v){
        Toast.makeText(MainActivity.this, "" + position, Toast.LENGTH_SHORT).show();
    }*/
}