package com.oconut.stonegame;

/**
 * Created by HaebinShin on 2014-11-15.
 */
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageAdapter extends BaseAdapter{
    private Context mContext;
    private Integer[] mThumbIds = {
            R.drawable.glass_numbers_0, R.drawable.glass_numbers_1,
            R.drawable.glass_numbers_2, R.drawable.glass_numbers_3,
            R.drawable.glass_numbers_4, R.drawable.glass_numbers_5,
            R.drawable.glass_numbers_6, R.drawable.glass_numbers_7,
            R.drawable.glass_numbers_8, R.drawable.glass_numbers_9,
            R.drawable.glass_numbers_2, R.drawable.glass_numbers_5,
            R.drawable.glass_numbers_1, R.drawable.glass_numbers_7,
            R.drawable.glass_numbers_0, R.drawable.glass_numbers_9,
            R.drawable.glass_numbers_2, R.drawable.glass_numbers_5,
            R.drawable.glass_numbers_4, R.drawable.glass_numbers_9,
            R.drawable.glass_numbers_0, R.drawable.glass_numbers_1
    };

    public ImageAdapter(Context c){
        mContext = c;
    }
    public int getCount(){
        return mThumbIds.length;
    }
    public Object getItem(int position){
        return null;
    }
    public long getItemId(int position){
        return 0;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        ImageView imageView;
        if(convertView == null){
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        }
        else{
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(mThumbIds[position]);
        return imageView;
    }

}